
$('.menuLink').click(function(){
	var linkClicked = $(this).text().toLowerCase().replace(/ /g, "-");
	
	$(".menuLink").removeClass('selected');
	$(this).addClass('selected');
	$(".menuImage:visible").hide();
	$("[data-menuname="+linkClicked+"]").removeClass('hidden');
	$("[data-menuname="+linkClicked+"]").show();
})