<?php


class Menu extends AppModel
{
	function getAllMenus($restaurantId) {
		$menus = Cache::read('menus_'.$restaurantId, 'longterm');
		if (! $menus) 
		{
			$menus = $this->find('all', array('conditions' => array('Menu.restaurant_id' => $restaurantId)));
			Cache::write('menus_'.$restaurantId, $menus, 'longterm');
		}
		return $menus;
	}
	
	function getAllActiveMenus($restaurantId) {
		$menus = Cache::read('active_menus_'.$restaurantId, 'longterm');
		if (! $menus)
		{
			$menus = $this->find('all', array('conditions' => array('Menu.restaurant_id' => $restaurantId, 'Menu.is_active' => true)));
			Cache::write('active_menus_'.$restaurantId, $menus, 'longterm');
		}
		return $menus;
	}
	
	
}