<?php $count = 0;
	$limit_five = 0;
?>

<div class="container-fluid homepage">
	<div class="row-fluid">
    	<div></div>
   </div><!--/row-->
   
  <div class="row-fluid">
  			<div class="span4">
  				<p class="home">
  					<span id="main-blurb">CapitalMenus.com</span>
  					is your source for the most up-to-date and accessible 
  					menus and restaurant information for the Capital Region of NY.</p>
  			</div>
  
	      <?php $city_name = 'Albany'; ?>
		      <div class="span4">
		          <h2 class="home">
		          	<?php echo $this->Html->link('Albany', 
		          		array('controller' => 'restaurants', 'action' => 'viewByCity', 'Albany'), 
		          		array('title' => 'View all Albany restaurants')); 
		          	?>
		          </h2>
		          <ul>
	
			      <?php foreach ($all_restaurants as $restaurant): ?>
				  	<?php if ($restaurant['Restaurant']['city'] != $city_name): ?>
				  		<?php $count++; ?>
				  		<?php $limit_five = 0; ?>
				  		<?php $city_name = $restaurant['Restaurant']['city']; ?>
				          </ul>
				      </div>
				      <div class="span4" style="<?php echo ($count % 3 == 0) ? 'margin-left:0;': ''; ?>">
				      	<h2 class="home">
				      		<?php echo $this->Html->link($city_name, 
				      		array('controller' => 'restaurants', 'action' => 'viewByCity', $city_name),
				      		array('title' => 'View all '.$city_name. ' restaurants')); ?>
				      	</h2>
				      	<ul>
					  	
					<?php else: ?>
						<?php if ($limit_five < 5): ?>
							<li>
								<?php echo $this->Html->link($restaurant['Restaurant']['name'], 
							   						array('controller' => 'menus', 'action' => 'index', $restaurant['Restaurant']['id']),
							   						array('title' => "View menus")); ?>
							</li>
							
						<?php endif; ?>
						<?php if ($limit_five == 5): ?>
							<li>
							<?php echo $this->Html->link('View more '.$restaurant['Restaurant']['city'].' restaurants',
										array('controller' => 'restaurants', 'action' => 'viewByCity', $city_name),
							   						array('title' => "View all restaurants", 'class' => 'viewmore')); ?>
							</li>
						<?php endif; ?>
						<?php $limit_five++; ?>
					<?php endif; ?>
					
			      <?php endforeach; ?>
     
      </div>

     
</div><!--/.fluid-container-->