<?php
	$to_replace = array(" ", "&");
	$replace_with = array("", "");
	$num_restaurants = count($restaurants);
	$rest_count = 0;
	
?>


<div class="container-fluid">
	<div class="row-fluid">
    	<div></div><!--/span-->
        <div class="span8">
        	<div class="row-fluid">
				<div>
		    		<h2 id="<?php echo $current_city; ?>" class="rest-header"><?php echo $current_city; ?> Restaurants</h2>
		    		<br>
		    		<div id="main-container">
					   <ul>
					   <?php foreach ($restaurants as $restaurant): ?>
					   		<li>
					   			<div class="restaurant">
					   				<?php echo $this->Html->link($restaurant['Restaurant']['name'], 
					   						array('controller' => 'menus', 'action' => 'index', 
					   							 $restaurant['Restaurant']['id'], $current_city, $restaurant['Restaurant']['name']),
					   						array('title' => "View menus", 'class' => 'restaurant_name')); ?>
					   				<span class="phone"><?php echo $restaurant['Restaurant']['phone']; ?></span>
					   				<span class="cuisine"><?php echo $restaurant['Restaurant']['cuisine']; ?></span>
					   				<span class="address">
					   					<a href="http://maps.google.com/?q=<?php echo $restaurant['Restaurant']['address1']." ". $current_city. ", NY"; ?>" target="_blank">
					   						<?php echo $restaurant['Restaurant']['address1']; ?>
					   					</a>
					   				</span>
					   				<?php if ($restaurant['Restaurant']['description'] != 'no description yet'): ?>
						   				<div class="description" id="description-<?php echo $restaurant['Restaurant']['id']; ?>">
						   					<?php 
						   						echo $restaurant['Restaurant']['description'];		
						   					?>
						   				</div>
					   				<?php endif; ?>
					   			</div>
					   		</li>
					   		<?php $rest_count++; ?>
					   		
					   		<?php if ($rest_count < $num_restaurants): ?>
					   			<hr>
					   		<?php endif; ?>

					   <?php endforeach; ?>
					   </ul>
					</div>
				            				   
				</div><!--/span-->
			</div><!--/row-->
          
          
       </div><!--/span-->
   </div><!--/row-->
     
</div><!--/.fluid-container-->

<?php echo $this->Html->script('restaurant');

