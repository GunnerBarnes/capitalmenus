<?php 
	$existingMenus = array();
?>


<div class="container-fluid">
	<div class="row-fluid">
    	<div></div><!--/span-->
        <div class="span12">
			<div class="restaurant-container span12">
				<div class="restaurant-info row-fluid">
					<div class="address span7">
						<div class="name">
							<?php echo $restaurant['Restaurant']['name']; ?>
						</div>
						<div class="line1">
							<?php echo $restaurant['Restaurant']['address1']; ?>
						</div>
						<div class="line2">
							<?php echo $restaurant['Restaurant']['city'].', NY'; ?>
							<?php echo $restaurant['Restaurant']['zip']; ?>
						</div>
					</div>
					<div class="details span4">
						<div class="phone"><?php echo $restaurant['Restaurant']['phone']; ?></div>
						<div class="website">
							<a target="_blank" href=<?php echo $restaurant['Restaurant']['website']; ?>>
								<?php echo substr($restaurant['Restaurant']['website'], 7); ?>
							</a>
						</div>
						<div class="cuisine">Cuisine: <?php echo $restaurant['Restaurant']['cuisine']; ?></div>
					</div>
				</div>
				
				
				<div class="menus">
					<ul id="menus" class="menu-nav">
						<?php if(count($menus) > 0): ?>
							<li>Menus: </li>
						<?php else: ?>
							<li class="nomenu">No menus available yet</li>
							<li class="nomenu submit-menu">Have a menu for this restaurant that you would like featured on the site?</li>
							<li class="nomenu submit-menu">Send it to <a href="mailto:info@capitalmenus.com">info@capitalmenus.com</a></li>
						<?php endif; ?>
						
						<?php foreach($menus as $menu): ?>
							<?php if(! in_array($menu['Menu']['name'], $existingMenus)): 
								array_push($existingMenus, $menu['Menu']['name']);
							?>
								<li>
									<a href="#" class="menuLink <?php echo ($menu['Menu']['is_default'] == "1") ? ' selected' : ''; ?>">
										<?php echo ucwords($menu['Menu']['name']); ?>
									</a>
								</li>
							<?php endif; ?>
						<?php endforeach; ?>
					</ul>
					
					<div id="menu-container">
						<?php foreach($menus as $menu): ?>
							<div class="menuImage <?php echo ($menu['Menu']['is_default'] == "1") ? '' : ' hidden'; ?>" 
								 data-menuname="<?php echo str_replace(' ', '-', $menu['Menu']['name']); ?>">
								<?php echo $this->Html->image($menu['Menu']['image_url']); ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
        </div>
	</div>
</div>

<?php echo $this->Html->script('menus');