
<div class="container-fluid">
	<div class="row-fluid">
    	<div class="span6">
	    	<p>CapitalMenus.com is focused on providing the best restaurant search experience for the NY Capital Region. If a restaurant you are
	    	looking for is not listed here, or for any other inquiries, contact us at <a href="mailto:info@capitalmenus.com">info@capitalmenus.com</a>.</p>
    	</div>
    </div>
</div>
      