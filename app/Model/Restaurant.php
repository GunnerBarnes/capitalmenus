<?php


class Restaurant extends AppModel
{	
	function getAllRestaurantsForCity($cityID){
		$restaurants = Cache::read('restaurants_'.$cityID, 'longterm');
		if (!$restaurants)
		{
			$restaurants = $this->find('all', array('conditions' => array('Restaurant.city' => $cityID, 'Restaurant.is_active' => 1), 'order' => 'name'));
			Cache::write('restaurants_'.$cityID, $restaurants, 'longterm');
		}
		return $restaurants;
	}
	
	function getAllRestaurants() {
		$restaurants = Cache::read('all_restaurants', 'longterm');
		if (!$restaurants)
		{
			$restaurants = $this->find('all', array('conditions' => array('Restaurant.is_active' => 1), 'order' => array('city', 'rand()')));
			Cache::write('all_restaurants', $restaurants, 'longterm');
		}
		return $restaurants;
	}
	
	function getRestaurant($restaurantId){
		$restaurant = Cache::read('restaurant_'.$restaurantId, 'longterm');
		if (!$restaurant)
		{
			$restaurant = $this->find('all', array('conditions' => array('Restaurant.id' => $restaurantId)));
			Cache::write('restaurant_'.$restaurantId, $restaurant, 'longterm');
		}
		return $restaurant;
	}
}