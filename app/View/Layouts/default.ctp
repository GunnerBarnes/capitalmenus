<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $title_for_layout; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="">
    <meta name="keywords" content="restaurants in albany ny, italian restaurants albany ny, albany restaurants, clifton park restaurants, restaurants in clifton park, schenectady restaurants, restaurants in schenectady ny, best restaurants in albany ny, menus albany ny"/>
    <meta name="description" content="Capital District restaurant information and menus, all in one place."
    
    <?php
                echo $this->Html->meta('favicon.ico','../img/favicon.ico',array('type' => 'icon'));

                echo $this->Html->css('bootstrap.min');
                echo $this->Html->css('bootstrap-responsive.min');
                echo $this->Html->css('style');
                echo $this->Html->css('city');
                echo $this->Html->css('restaurant');
                echo $this->Html->script('jquery-1.10.2.min.js');
                echo $this->Html->script('jquery.expander.min.js');
                
                
                echo $this->fetch('meta');
                echo $this->fetch('css');
                echo $this->fetch('script');
        ?>


    <!-- Le styles -->
    <style type="text/css">
          </style>
<!--     <link href="~webroot/css/bootstrap-responsive.css" rel="stylesheet"> -->

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->
                       
	
	<!-- Google Analytics -->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-38047239-1', 'capitalmenus.com');
	  ga('send', 'pageview');
	
	</script>	
	<!-- End Google Analytics -->
	
  </head>

  <body>
    <!-- Google Tag Manager -->
  <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5ZXPCR"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-5ZXPCR');</script>
  <!-- End Google Tag Manager -->


    <div class="container">
		<?php echo $this->element('header'); ?>
		
		<div align="center">
       <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
           <!-- topbanner -->
           <ins class="adsbygoogle"
                style="display:inline-block;width:728px;height:90px"
                data-ad-client="ca-pub-1812435568229269"
                data-ad-slot="2576745632"></ins>
       <script>
           (adsbygoogle = window.adsbygoogle || []).push({});
       </script>
      
	    <?php print $this->fetch("content"); ?>
	  
	  <div class="push"></div>
    </div> <!-- /container -->
    

	<?php 
		echo $this->element('footer'); 
		echo $this->Js->writeBuffer(); 

	 	echo $this->Html->script('jquery-1.10.2.min.js');
        echo $this->Html->script('jquery.expander.min.js');
    ?>
	
  </body>

</html>
