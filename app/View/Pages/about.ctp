<div class="container-fluid">
	<div class="row-fluid">
    	<div class="span6">
	    	<p>CapitalMenus.com aims to provide up-to-date restaurant and menu information for the NY Capital Region, to help visitors and residents easily explore the many fantastic local dining options. With 150+ restaurant listings and menus, CapitalMenus.com is committed to bringing you the easiest and most satisfying source for all of your dining decision-making needs.</p>
    	</div>
    </div>
</div>
      