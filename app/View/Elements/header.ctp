<div class="header">
    <h1 class="home">
        <?php echo $this->Html->link('Capital Menus', array('controller' => 'pages', 'action' => 'index')); ?>
    </h1>
    <div class="span9">
        <ul class="nav">
        	<li><?php echo $this->Html->link('Albany', array('controller' => 'restaurants', 'action' => 'viewByCity', 'Albany')); ?></li>
        	<li><?php echo $this->Html->link('Clifton Park', array('controller' => 'restaurants', 'action' => 'viewByCity', 'Clifton Park')); ?></li>
        	<li><?php echo $this->Html->link('Colonie', array('controller' => 'restaurants', 'action' => 'viewByCity', 'Colonie')); ?></li>
        	<li><?php echo $this->Html->link('Rensselaer', array('controller' => 'restaurants', 'action' => 'viewByCity', 'Rensselaer')); ?></li>
        	<li><?php echo $this->Html->link('Saratoga', array('controller' => 'restaurants', 'action' => 'viewByCity', 'Saratoga')); ?></li>
        	<li><?php echo $this->Html->link('Schenectady', array('controller' => 'restaurants', 'action' => 'viewByCity', 'Schenectady')); ?></li>
        	<li><?php echo $this->Html->link('Troy', array('controller' => 'restaurants', 'action' => 'viewByCity', 'Troy')); ?></li>
        </ul>
        <?php  
		    echo $this->form->create(null, array(
				'url' => array('controller' => 'pages', 'action' => 'search'), 'id' => 'RestaurantSearchForm'));
		    echo $this->form->input("keywords", array('label' => false)); 
		    echo $this->form->end("Search"); 
		?> 
    </div>
</div>
