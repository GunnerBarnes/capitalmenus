
$('div.description').expander({
  slicePoint: 200,
  preserveWords: true,
  widow: 2,
  expandEffect: 'show',
  userCollapseText: 'show less',
  expandEffect: 'fadeIn',
  expandSpeed: 500,
  collapseEffect: 'fadeOut',
  collapseSpeed: 200
});