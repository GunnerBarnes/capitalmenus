<?php
App::uses('AppController', 'Controller');

class MenusController extends AppController {
	
	var $name = 'Menus';

    public $helpers = array('Html', 'Form', 'Cache');
    
    var $uses = array('Menu', 'Restaurant');
    
    public $cacheAction = array('index' => '+1 day');
    
    public function index($arg1 = 1, $arg2 = '', $arg3 = '') {
	    $this->set('restaurant_id', $arg1);
	    
	    $menus = $this->Menu->getAllActiveMenus($arg1);
	    $restaurant = $this->Restaurant->getRestaurant($arg1);
	    
	    $menu_ids = array();
	    foreach ($menus as $menu) {
		    array_push($menu_ids, $menu['Menu']['id']);
	    }
	    
	    
	    $this->set('menus', $menus);
	    $this->set('restaurant', $restaurant[0]);
	    $this->set('title_for_layout', $restaurant[0]['Restaurant']['name'].' '.$restaurant[0]['Restaurant']['city'].' NY | CapitalMenus.com');
    }
    
}