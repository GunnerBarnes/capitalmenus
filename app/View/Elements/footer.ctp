<div class="footer">
	<ul class="nav">
		<li>&copy;2013 Capital Menus</li>
		<li>
			<?php echo $this->Html->link('Contact', 
				array('controller' => 'pages', 'action' => 'contact'), 
				array('title' => 'Contact Capital Menus')); 
			?>
		</li>
		<li>
			<?php echo $this->Html->link('About Us', 
				array('controller' => 'pages', 'action' => 'about'), 
				array('title' => 'About Capital Menus')); 
			?>
		</li>
	</ul>
</div>