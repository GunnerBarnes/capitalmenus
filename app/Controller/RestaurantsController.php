<?php
App::uses('AppController', 'Controller');

class RestaurantsController extends AppController {

	
	var $name = 'Restaurants';

    var $helpers = array('Html', 'Form', 'Cache');
    
    var $uses = array('Restaurant');
    
    

    public $cacheAction = array('index' => '+1 day', 'viewByCity' => '+1 day');
    
    public function index() {
	    $this->set('restaurants', $this->Restaurant->getAllRestaurants());
    }
    
    public function viewByCity($arg1 = 'Troy') {
    	$this->set('current_city', $arg1);
    
		$restaurants = $this->Restaurant->getAllRestaurantsForCity($arg1);
		
		// here we get all restaurant ids for our city so we can get just show those menus
		$restaurant_ids = array();
		foreach ($restaurants as $restaurant) {
			array_push($restaurant_ids, $restaurant['Restaurant']['id']);
		}
		
		$this->set('restaurants', $restaurants);
		$this->set('title_for_layout', $arg1.', NY Menus | CapitalMenus.com');
    }
    
}